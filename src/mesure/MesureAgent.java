package mesure;

import jade.core.AID;
import jade.core.Agent;

public class MesureAgent extends Agent {
	
	private AID console;
	
	public void setup() {
		Object[] arg = getArguments();
		this.setConsole((AID)arg[0]);
		addBehaviour(new MesureBehaviour());
	}

	public AID getConsole() {
		return console;
	}

	public void setConsole(AID console) {
		this.console = console;
	}

}
