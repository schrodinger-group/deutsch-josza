package mesure;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import utils.IntricatedQubits;

public class MesureBehaviour extends CyclicBehaviour {
	
	@Override
	public void action() {
		
		ACLMessage msg = super.myAgent.receive(MessageTemplate.MatchPerformative(ACLMessage.REQUEST));
		if (msg != null) {
			
			ObjectMapper mapper = new ObjectMapper();
			try {
				IntricatedQubits qubits = mapper.readValue(msg.getContent(), IntricatedQubits.class);
				ArrayList<Double> items = qubits.coeffs;
				double value = 0;
				while (value == 0)
					value = (double)Math.round(Math.random() * 10d) / 10d;
				double square = 0; 
				int i=0;
				while(value > square && i < items.size()) {
					square+=Math.pow(items.get(i++),2);
				}
						
				ACLMessage message = new ACLMessage (ACLMessage.INFORM);
						
				message.setContent(Integer.toString(--i));
				message.addReceiver(((MesureAgent)super.myAgent).getConsole());
				super.myAgent.send(message); 
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		} else block();
			

	}

}
