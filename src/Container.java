import java.net.InetAddress;

import jade.core.AID;
import jade.core.Profile;
import jade.core.ProfileException;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import utils.AlgoSettings;
import utils.AlgoSettings.OracleType;

public class Container {
	public static String MAIN_PROPERTIES_FILE="config2";

	public static void main(String[] args) throws Exception {
		Runtime rt = Runtime.instance();
		Profile p = new ProfileImpl(MAIN_PROPERTIES_FILE);
		String IP=InetAddress.getLocalHost().getHostAddress();
		p.setParameter("host", IP);
		ContainerController cc = rt.createAgentContainer(p);
		
		AgentController alice = cc.createNewAgent("AliceAgent", "alice.AliceAgent", null);
		alice.start(); 
		try{
		}
		catch(Exception ex){
			ex.printStackTrace();
		} 
	}

}
