package hadamard;

import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.ObjectMapper;

import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.proto.AchieveREInitiator;

public class GetHadamardMatrix extends AchieveREInitiator {

	public GetHadamardMatrix(Agent agent, ACLMessage msg) {
		super(agent, msg);	
	}
	
	@Override
	protected void handleInform(ACLMessage response) {
		ObjectMapper mapper = new ObjectMapper();
		ArrayList<ArrayList<ArrayList<Double>>> matrixHadamard;
		try {
			matrixHadamard = mapper.readValue(response.getContent(), mapper.getTypeFactory().constructCollectionType(ArrayList.class, mapper.getTypeFactory().constructCollectionType(ArrayList.class, mapper.getTypeFactory().constructCollectionType(ArrayList.class, Double.class))));
			((HadamardAgent)super.myAgent).setMatrix(matrixHadamard.get(0));
			super.myAgent.addBehaviour(new HadamardWaitCoeffsBehaviour(super.myAgent));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
