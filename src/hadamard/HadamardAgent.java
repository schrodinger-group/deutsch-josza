package hadamard;
import java.util.ArrayList;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;

public class HadamardAgent extends Agent {
	private static final long serialVersionUID = 1L;
	public String aidAgent;
	private ArrayList<ArrayList<Double>> matrix;
	protected void setup() {
		Object[] args = getArguments();
		this.aidAgent = (String)args[0];
		try{
			DFAgentDescription template = new DFAgentDescription();
			ServiceDescription sd = new ServiceDescription();
			sd.setType("MatrixMap");
			template.addServices(sd);
			DFAgentDescription[] result = DFService.search(this, template);
			ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
			AID library = result[0].getName();
			msg.addReceiver(library);
			ArrayList<String> content = new ArrayList<>();
			content.add("hadamard");
			ObjectMapper mapper = new ObjectMapper();
			msg.setContent(mapper.writeValueAsString(content));
			addBehaviour(new GetHadamardMatrix(this, msg));
		}
		catch (FIPAException | JsonProcessingException ex) {
			ex.printStackTrace();
		}
	}
	
	public String getAidAgent() {
		return this.aidAgent;
	}
	
	public void setMatrix(ArrayList<ArrayList<Double>> matrix) {
		this.matrix = matrix;
	}
	
	public ArrayList<ArrayList<Double>> getMatrix() {
		return matrix;
	}

}