package hadamard;

import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.ObjectMapper;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import utils.IntricatedQubits;


public class HadamardWaitCoeffsBehaviour extends CyclicBehaviour {
		
	public HadamardWaitCoeffsBehaviour(Agent a) {
		super.myAgent = a;
	}
	@Override
	public void action() {	
		MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.REQUEST);
		ACLMessage message = super.myAgent.receive(mt);
		if (message != null) {
			String content = message.getContent();
			ObjectMapper mapper = new ObjectMapper();
			IntricatedQubits inputQubit;
			try {
				inputQubit = mapper.readValue(content, IntricatedQubits.class); 
				if (inputQubit.coeffs.size() == 2 && inputQubit.verifierCoeff()) {
					ArrayList<ArrayList<Double>> matrix = ((HadamardAgent)super.myAgent).getMatrix();
					double a = inputQubit.coeffs.get(0);
					double b = inputQubit.coeffs.get(1);
					double aResult = a * matrix.get(0).get(0) + b * matrix.get(0).get(1);
					double bResult = a * matrix.get(1).get(0) + b * matrix.get(1).get(1);
					ArrayList<Double> outputCoeffsQubit = new ArrayList<>();
					outputCoeffsQubit.add(aResult);
					outputCoeffsQubit.add(bResult);
					ACLMessage messageCoeffs = new ACLMessage(ACLMessage.REQUEST);
					String aidAgent = ((HadamardAgent)super.myAgent).getAidAgent();
					messageCoeffs.addReceiver(new AID(aidAgent, AID.ISLOCALNAME));
					IntricatedQubits coeffsObjetResult = new IntricatedQubits();
					coeffsObjetResult.coeffs = outputCoeffsQubit;
					String s_coeffs = "";
					s_coeffs = mapper.writeValueAsString(coeffsObjetResult);
					messageCoeffs.setContent(s_coeffs);
					super.myAgent.send(messageCoeffs);
				} else {
					System.out.println("Nombre de coefficients pas bon");
				}
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		} else
			block();
	}
}
