package multiplication;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;

public class MultiplicationAgent extends Agent {
		
	private int sId = 0;
	private Map<Integer, ArrayList<Double>> vectors = new HashMap<Integer, ArrayList<Double>>();
	private Map<Integer, ACLMessage> messages = new HashMap<Integer, ACLMessage>();

	public void setup() {
		inscription();
		addBehaviour(new WaitForMultiplication(this));
	}
	
	private void inscription() {
		DFAgentDescription dfad = new DFAgentDescription();
		dfad.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("Multiplication");
		sd.setName(getLocalName());
		dfad.addServices(sd);
		try { 
			DFService.register(this, dfad);
		} catch (FIPAException fe) {
			fe.printStackTrace(); 
		}
	}
	
	protected void takeDown() {
       try { DFService.deregister(this); }
       catch (Exception e) {}
    }
	
	public void newId() {
		this.sId++;
	}
	
	public int getId() {
		return sId;
	}
	
	public void addToMap(ACLMessage msg, int size) {
		ArrayList<Double> vec = new ArrayList<Double>();
		for (int i=0; i<size; i++)
			vec.add(null);
		vectors.put(sId, vec);
		messages.put(sId, msg);
	}
	
	public void removeFromMap(Integer id) {
		vectors.remove(id);
		messages.remove(id);
	}
	
	public boolean updateValues(Integer agentIndex, Integer valIndex, Double value) {
		vectors.get(agentIndex).set(valIndex, value);
		for (Double d : vectors.get(agentIndex))
			if (d == null)
				return false;
		return true;
	}

	public ACLMessage getMessage(Integer index) {
		return messages.get(index);
	}

	public ArrayList<Double> getVector(Integer index) {
		return vectors.get(index);
	}
}
