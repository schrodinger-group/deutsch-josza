package multiplication;
import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.wrapper.StaleProxyException;
import utils.Tuple;

public class SendResult extends OneShotBehaviour {

	private Integer index;
	
	public SendResult(Agent agent, Integer index) {
		super.myAgent = agent;
		this.index = index;
	}
	
	@Override
	public void action() {
		ACLMessage msg = ((MultiplicationAgent)super.myAgent).getMessage(index).createReply();
		msg.setPerformative(ACLMessage.INFORM);
		ArrayList<Double> vec = ((MultiplicationAgent)super.myAgent).getVector(index);
		ObjectMapper mapper = new ObjectMapper();
		((MultiplicationAgent)super.myAgent).removeFromMap(index);
		try {
			msg.setContent(mapper.writeValueAsString(vec));
			super.myAgent.send(msg);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}
}
