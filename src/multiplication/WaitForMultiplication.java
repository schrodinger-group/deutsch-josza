package multiplication;
import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.ObjectMapper;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.wrapper.StaleProxyException;
import utils.Tuple;

public class WaitForMultiplication extends CyclicBehaviour {

	public WaitForMultiplication(Agent agent) {
		super.myAgent = agent;
	}
	
	@Override
	public void action() {
		ACLMessage msg = super.myAgent.receive(MessageTemplate.MatchPerformative(ACLMessage.REQUEST));
		if (msg != null) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				ArrayList<ArrayList<Double>> matrix = mapper.readValue(msg.getContent(), mapper.getTypeFactory().constructCollectionType(ArrayList.class, mapper.getTypeFactory().constructCollectionType(ArrayList.class, Double.class)));
				ArrayList<Double> vect = matrix.remove(matrix.size()-1);
				DFAgentDescription template = new DFAgentDescription();
				ServiceDescription sd = new ServiceDescription();
				sd.setType("Analyse");
				template.addServices(sd);
				DFAgentDescription[] result = DFService.search(super.myAgent, template);
				if (result.length > 0) {
					((MultiplicationAgent)super.myAgent).addToMap(msg, matrix.size());
					String id = Integer.toString(((MultiplicationAgent)super.myAgent).getId());
					((MultiplicationAgent)super.myAgent).newId();
					for (int i = 0; i < matrix.size(); i++) {
						AID analyse = result[i%result.length].getName();
						Tuple<Double> tuple = new Tuple<Double>();
						tuple.setList1(matrix.get(i));
						tuple.setList2(vect);
						String content = mapper.writeValueAsString(tuple);
						ACLMessage analyseMsg = new ACLMessage(ACLMessage.REQUEST);
						analyseMsg.setContent(content);
						analyseMsg.addReceiver(analyse);
						analyseMsg.setConversationId(Integer.toString(i)+"-"+id);
						super.myAgent.addBehaviour(new WaitForReply(super.myAgent, analyseMsg));
					}
					
				}	
			} catch (IOException | FIPAException e) {
				e.printStackTrace();
			}
		}
	}
}
