package multiplication;

import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.proto.AchieveREInitiator;

public class WaitForReply extends AchieveREInitiator {

	public WaitForReply(Agent agent, ACLMessage msg) {
		super(agent, msg);
	}
	
	@Override
	public void handleInform(ACLMessage msg) {
		String[] indices = msg.getConversationId().split("-");
		Integer valIndex = Integer.parseInt(indices[0]);
		Integer vecIndex = Integer.parseInt(indices[1]);
		Double val = Double.parseDouble(msg.getContent());
		if (((MultiplicationAgent)super.myAgent).updateValues(vecIndex, valIndex, val))
			super.myAgent.addBehaviour(new SendResult(super.myAgent, vecIndex));
	}
}
