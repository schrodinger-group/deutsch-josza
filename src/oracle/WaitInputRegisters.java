package oracle;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.databind.ObjectMapper;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import utils.IntricatedQubits;

public class WaitInputRegisters extends Behaviour {

	public WaitInputRegisters(Agent agent) {
		super.myAgent = agent;
	}
	
	@Override
	public void action() {
		ACLMessage msg = super.myAgent.receive(MessageTemplate.MatchPerformative(ACLMessage.REQUEST));
		if (msg != null) {
			ObjectMapper mapper = new ObjectMapper();
			String sender = msg.getSender().getLocalName();
			int index = -1;
			try {
				IntricatedQubits qubit = mapper.readValue(msg.getContent(), IntricatedQubits.class);
				if (qubit.coeffs.size() == 2) {
					String pattern = "^"+((OracleAgent)super.myAgent).getInputBaseName()+"([0-9]+)$";
					Pattern r = Pattern.compile(pattern);
					Matcher m = r.matcher(sender);
					if (m.find()) {
						index = Integer.parseInt(m.group(1));
					}
					else if (msg.getSender() == ((OracleAgent)super.myAgent).getConsoleAgent())
						index = ((OracleAgent)super.myAgent).getRegisters().size()-1;
					if (index >= 0) 
						((OracleAgent)super.myAgent).getRegisters().set(index, new IntricatedQubits(qubit.coeffs));
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean done() {
		for (IntricatedQubits r : ((OracleAgent)super.myAgent).getRegisters())
			if (r == null)
				return false;
		return true;
	}
}
