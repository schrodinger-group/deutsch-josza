package oracle;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import multiplication.MultiplicationAgent;
import utils.IntricatedQubits;

public class ComputeResult extends OneShotBehaviour {

	public ComputeResult(Agent agent) {
		super.myAgent = agent;
	}
	
	@Override
	public void action() {
		try {
			ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
			ObjectMapper mapper = new ObjectMapper();
			ArrayList<ArrayList<Double>> matrix = (ArrayList<ArrayList<Double>>) ((OracleAgent)super.myAgent).getMatrix().clone();
			ArrayList<Double> vector = ((OracleAgent)super.myAgent).getInitialState().coeffs;
			matrix.add(vector);
			String serMsg = mapper.writeValueAsString(matrix);
			msg.setContent(serMsg);
			DFAgentDescription template = new DFAgentDescription();
			ServiceDescription sd = new ServiceDescription();
			sd.setType("Multiplication");
			template.addServices(sd);
			DFAgentDescription[] result = DFService.search(super.myAgent, template);
			if (result.length > 0) {
				msg.addReceiver(result[0].getName());
				super.myAgent.addBehaviour(new SendResult(super.myAgent, msg));
			}
		} catch (JsonProcessingException | FIPAException e) {
			e.printStackTrace();
		}
	}
}
