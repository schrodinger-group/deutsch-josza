package oracle;

import static java.lang.Math.pow;

import java.util.ArrayList;
import java.util.Random;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SequentialBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import utils.AlgoSettings;
import utils.IntricatedQubits;

public class OracleAgent extends Agent {
	
	private String inputBaseName;
	private AID consoleAgent, nHadamardAgent;
	private ArrayList<IntricatedQubits> registers = new ArrayList<IntricatedQubits>();
	private ArrayList<ArrayList<Double>> matrix;
	private IntricatedQubits initialState;
	
	protected void setup(){
		ACLMessage retrieveMatrixMsg = new ACLMessage(ACLMessage.REQUEST);
		ArrayList<String> msgContent = new ArrayList<String>();
		msgContent.add("paulix");
		msgContent.add("identity");
		
		DFAgentDescription template = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		sd.setType("MatrixMap");
		template.addServices(sd);
		DFAgentDescription[] result;
		try {
			result = DFService.search(this, template);
			AID library = result[0].getName();
			retrieveMatrixMsg.addReceiver(library);
		} catch (FIPAException e1) {
			e1.printStackTrace();
		}
		
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			retrieveMatrixMsg.setContent(mapper.writeValueAsString(msgContent));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		SequentialBehaviour seq = new SequentialBehaviour();
		seq.addSubBehaviour(new ConstructOracle(this, retrieveMatrixMsg, getArguments()));
		seq.addSubBehaviour(new WaitInputRegisters(this));
		seq.addSubBehaviour(new MergeRegisters(this));
		seq.addSubBehaviour(new ComputeResult(this));
		addBehaviour(seq);
	}

	public String getInputBaseName() {
		return inputBaseName;
	}

	public void setInputBaseName(String inputBaseName) {
		this.inputBaseName = inputBaseName;
	}

	public AID getConsoleAgent() {
		return consoleAgent;
	}

	public void setConsoleAgent(AID consoleAgent) {
		this.consoleAgent = consoleAgent;
	}

	public AID getnHadamardAgent() {
		return nHadamardAgent;
	}

	public void setnHadamardAgent(AID nHadamardAgent) {
		this.nHadamardAgent = nHadamardAgent;
	}

	public ArrayList<IntricatedQubits> getRegisters() {
		return registers;
	}

	public void setRegisters(ArrayList<IntricatedQubits> registers) {
		this.registers = registers;
	}
	
	public ArrayList<ArrayList<Double>> getMatrix() {
		return matrix;
	}

	public void setMatrix(ArrayList<ArrayList<Double>> matrix) {
		this.matrix = matrix;
	}

	public IntricatedQubits getInitialState() {
		return initialState;
	}

	public void setInitialState(IntricatedQubits initialState) {
		this.initialState = initialState;
	}
	
	public void addRegister(IntricatedQubits reg) {
		this.registers.add(reg);
	}
	
}
