package oracle;

import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.proto.AchieveREInitiator;
import utils.IntricatedQubits;

public class SendResult extends AchieveREInitiator {

	public SendResult(Agent agent, ACLMessage msg) {
		super(agent, msg);
	}
	
	@Override
	public void handleInform(ACLMessage answer) {
		ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
		ObjectMapper mapper = new ObjectMapper();
		try {
			ArrayList<Double> msgContent = mapper.readValue(answer.getContent(), mapper.getTypeFactory().constructCollectionType(ArrayList.class, Double.class));
			IntricatedQubits state = new IntricatedQubits(msgContent);
			String serQubits = mapper.writeValueAsString(state);
			msg.setContent(serQubits);
			msg.addReceiver(((OracleAgent)super.myAgent).getnHadamardAgent());
			System.out.println("Phi2 = " + state.qubitsToString());
			super.myAgent.send(msg);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
}
