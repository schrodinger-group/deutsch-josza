package oracle;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.databind.ObjectMapper;

import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import utils.IntricatedQubits;

public class MergeRegisters extends OneShotBehaviour {

	public MergeRegisters(Agent agent) {
		super.myAgent = agent;
	}
	
	@Override
	public void action() {
		ArrayList<ArrayList<Double>> inputs = new ArrayList<>();
		ArrayList<IntricatedQubits> registers = ((OracleAgent)super.myAgent).getRegisters();
		for (IntricatedQubits r : registers)
			inputs.add(r.coeffs);
		ArrayList<Double> coeffs = getCoeff(inputs);
		((OracleAgent)super.myAgent).setInitialState(new IntricatedQubits(coeffs));
		System.out.println("Phi1 = " + ((OracleAgent)super.myAgent).getInitialState().qubitsToString());
	}
	
	private ArrayList<Double> getCoeff(ArrayList<ArrayList<Double>> lists) {
		ArrayList<Double> result = lists.get(0);
		ArrayList<Double> resultTemp = new ArrayList<Double>();
		for (int i = 1; i < lists.size(); i ++) {
				for (Double item1 : result) {
					for (Double item2 :  lists.get(i)) {
						resultTemp.add(item1 * item2);
					}
				}
				result = resultTemp;
				resultTemp = new ArrayList<Double>();
		}
		return result;
	}
}
