package oracle;

import static java.lang.Math.pow;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.proto.AchieveREInitiator;
import utils.AlgoSettings;
import utils.IntricatedQubits;

public class ConstructOracle extends AchieveREInitiator {

	private Object[] arg;
	
	public ConstructOracle(Agent agent, ACLMessage msg, Object[] arg) {
		super(agent, msg);
		this.arg = arg;
	}
	
	@Override
	public void handleInform(ACLMessage answer) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			ArrayList<ArrayList<ArrayList<Double>>> matrix = mapper.readValue(answer.getContent(), mapper.getTypeFactory().constructCollectionType(ArrayList.class, mapper.getTypeFactory().constructCollectionType(ArrayList.class, mapper.getTypeFactory().constructCollectionType(ArrayList.class, Double.class))));
			
			OracleAgent agent = (OracleAgent)super.myAgent;
			
			agent.setInputBaseName((String)arg[0]);
			Integer n = (Integer)arg[1];
			for (int i=0; i<=n; i++) {
				agent.addRegister(null);
			}
			agent.setConsoleAgent((AID)arg[2]);
			String nHadamardName = (String)arg[3];
			agent.setnHadamardAgent(new AID(nHadamardName, AID.ISLOCALNAME));
			AlgoSettings.OracleType type = (AlgoSettings.OracleType)arg[4];
			agent.setMatrix(funcToMatrix(createFunction(n, type), matrix));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private ArrayList<Double> createFunction(int n, AlgoSettings.OracleType type){
        switch (type) {
		case BALANCED:
			return balancedFunction(n);
		case CONSTANT:
			return constantFunction(n, -1);
		case ONE:
			return constantFunction(n, 1);
		case ZERO:
			return constantFunction(n, 0);
		default:
			return randomFunction(n);
        }
	}
	
	private ArrayList<Double> balancedFunction(int n) {
		ArrayList<Double> retour = new ArrayList<Double>();
		int a = (int)pow(2,n-1);
		int b = a;
        Random r = new Random();
        
		for(int i = 0; i < pow(2,n); i++){
			int o = r.nextInt(a + b);
			if (o < a){
				a--;
				retour.add(0.);
			} else {
				b--;
				retour.add(1.);
			}
    	}
    	return retour;
    }
	
	private ArrayList<Double> constantFunction(int n, int value) {
		ArrayList<Double> retour = new ArrayList<Double>();
		if (value != 0 && value != 1) {
			Random r = new Random();
	        value = r.nextInt(2);
		}
		for(int i = 0; i < pow(2,n); i++){
			retour.add((double)value);
		}
    	return retour;
    }
	
	public ArrayList<Double> randomFunction(int n) {
		Random r = new Random();
        int type = r.nextInt(2);     
        return type == 0 ? constantFunction(n, -1) : balancedFunction(n);
    }
	
	public ArrayList<ArrayList<Double>> funcToMatrix(ArrayList<Double> func, ArrayList<ArrayList<ArrayList<Double>>> HIMatrix) { 
		ArrayList<ArrayList<Double>> matrix = new ArrayList<ArrayList<Double>>();
		for (int i = 0; i < func.size(); i++) {
			ArrayList<Double> line1 = new ArrayList<Double>();
			ArrayList<Double> line2 = new ArrayList<Double>();
			for (int j = 0; j < 2*i; j++) {
				line1.add(0.);
				line2.add(0.);
			}
			Boolean isPauli = func.get(i) != 0;
			line1.add(isPauli ? HIMatrix.get(0).get(0).get(0) : HIMatrix.get(1).get(0).get(0));
			line1.add(isPauli ? HIMatrix.get(0).get(0).get(1) : HIMatrix.get(1).get(0).get(1));
			line2.add(isPauli ? HIMatrix.get(0).get(1).get(0) : HIMatrix.get(1).get(1).get(0));
			line2.add(isPauli ? HIMatrix.get(0).get(1).get(1) : HIMatrix.get(1).get(1).get(1));
			for (int j = 0; j < 2*(func.size()-i-1); j++) {
				line1.add(0.);
				line2.add(0.);
			}
			matrix.add(line1);
			matrix.add(line2);
		}
		return matrix;
	}
	
	
}
