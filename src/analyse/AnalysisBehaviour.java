package analyse;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import utils.Tuple;

public class AnalysisBehaviour extends CyclicBehaviour {
	private ACLMessage message;
	private ObjectMapper mapper;
	private int state;
	private Tuple<Double> lists;
	private int index;
	private double total;

	public AnalysisBehaviour(Agent myAgent){
		super.myAgent = myAgent;
	}
	
	@Override
	public void action() {
		switch(state) {
			case 0:
				message = myAgent.receive();
				if (message != null){
					this.mapper = new ObjectMapper();
					this.state = 0;
					this.lists = new Tuple<>();
					this.index = 0;
					this.total = 0;
					traitement(message);
					state = 1;
				} else {
					block();
				}
				break;
			case 1:
				iteration();
				break;
			case 2:
				reponse();
				state = 0;
				break;
		}
	}
	
	@SuppressWarnings("unchecked")
	public void traitement(ACLMessage message) {
		Tuple<Double> objets = new Tuple<>();
		try {
			objets = mapper.readValue(message.getContent().toString(), Tuple.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		lists = objets;
	}
	
	public void iteration() {
		if (lists.getList1().size() > index) {
			total += (lists.getElementList1(index) * lists.getElementList2(index));
			index++;
		} else {
			state = 2;
		}
	}
	
	public void reponse() {
		ACLMessage reponse = message.createReply();
		reponse.setContent("" + total);
		reponse.setPerformative(ACLMessage.INFORM);
		super.myAgent.send(reponse);
	}

}
