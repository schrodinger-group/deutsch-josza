package analyse;

import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

// L'agent analyse permet 
public class AnalysisAgent extends Agent {
	
	protected void setup(){
		inscription();
		addBehaviour(new AnalysisBehaviour(this));
	}
	
	private void inscription() {
		DFAgentDescription dfad = new DFAgentDescription();
		dfad.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("Analyse");
		sd.setName(getLocalName());
		dfad.addServices(sd);
		try { 
			DFService.register(this, dfad);
		} catch (FIPAException fe) {
			fe.printStackTrace(); 
		}
	}
	protected void takeDown() {
       try { DFService.deregister(this); }
       catch (Exception e) {}
    }
}
