package alice;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import utils.IntricatedQubits;

// Behaviour responsable du lancement de l'exp�rience, apr�s initialisation finie
public class StartBehaviour extends Behaviour{
	ObjectMapper mapper;
	Boolean ok = false;
	Scanner scan;
	
	
	public StartBehaviour(Agent myAgent) {
		super(myAgent);
		this.mapper = new ObjectMapper();
	}

	@Override
	public void action() {
		ACLMessage message = this.myAgent.receive();
		
		if (message != null) {
			traitement(message);
		} else {
			block();
		}
		
	}
	//traitement du message re�u, pour en retirer la liste d'AID
	private void traitement(ACLMessage message) {		
		try {
			ArrayList<String> content = mapper.readValue(message.getContent(), mapper.getTypeFactory().constructCollectionType(ArrayList.class, String.class));
			ArrayList<AID> contacts = contact(content);
			/*******/
			// Ce morceau sert � bloquer la progression, afin de pouvoir mettre en place le sniffer
			System.out.println("Entrer ce que vous voulez pour d�marrer :");
			scan = new Scanner(System.in);
			while(!scan.hasNext());
			/*******/
			envoi0(contacts.subList(0, contacts.size()-1));
			envoi1(contacts.get(contacts.size()-1));
			displayInitState(contacts.size()-1);
			ok = true;
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	//Affichage de l'�tat des qubits au d�part
	private void displayInitState(int n) {
		System.out.print("Phi0 = |");
		for (int i=0; i<n; i++)
			System.out.print(0);
		System.out.println("1>");
	}
	
	// exploitation de la liste des AID � contacter pour lancer l'exp�rience
	private ArrayList<AID> contact(ArrayList<String> names){
		ArrayList<AID> contacts = new ArrayList<>();
		
		for(String name : names) {
			contacts.add(this.myAgent.getAID(name));
		}
		return contacts;
	}
	
	// envoie des n qubits � 0 aux agent Hadamard donn�s par la liste d'AID
	private void envoi0(List<AID> contacts) {
		ArrayList<Double> coeff = new ArrayList<>();
		coeff.add(1.0);
		coeff.add(0.0);
		IntricatedQubits q = new IntricatedQubits(coeff);
		String content = "";
		try {
			content = mapper.writeValueAsString(q);
		} catch (Exception e) {
			e.printStackTrace();
		}
		for (AID dest : contacts) {
			ACLMessage message = new ACLMessage(ACLMessage.REQUEST);
			message.setContent(content);
			message.addReceiver(dest);
			this.myAgent.send(message);
		}
	}
	
	// Envoi du qubit initialis� � 1 � la derni�re porte d'hadamard
	private void envoi1(AID contact) {
		ArrayList<Double> coeff = new ArrayList<>();
		coeff.add(0.0);
		coeff.add(1.0);
		IntricatedQubits q = new IntricatedQubits(coeff);
		String content = "";
		try {
			content = mapper.writeValueAsString(q);
		} catch (Exception e) {
			e.printStackTrace();
		}
		ACLMessage message = new ACLMessage(ACLMessage.REQUEST);
		message.setContent(content);
		message.addReceiver(contact);
		this.myAgent.send(message);
	}

	@Override
	public boolean done() {
		// TODO Auto-generated method stub
		return ok;
	}

}
