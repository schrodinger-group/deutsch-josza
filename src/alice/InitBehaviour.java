package alice;

import java.util.Scanner;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import utils.AlgoSettings;
import utils.AlgoSettings.OracleType;

// Behaivour servant � interagir avec l'utilisateur via la console, afin de parametrer l'exp�rience.
public class InitBehaviour extends Behaviour {
	private int state;
	private Scanner scan;
	private AlgoSettings settings;
	
	public InitBehaviour(Agent myAgent) {
		super(myAgent);
		this.settings = new AlgoSettings();
	}
	
	@Override
	public void action() {
		switch(state) {
			case 0:
				System.out.println("Renseignez le n voulu : ");
				scan = new Scanner(System.in);
				state = 1;
				break;
			case 1:
				if (scan.hasNext()) {
					if (scan.hasNextInt()) {
						int a = scan.nextInt();
						if (a > 0) {
							this.settings.setSize(a);
							state = 2;
						} else {
							System.out.println("Veuillez renseigner un entier positif");
							scan = new Scanner(System.in);
						}
					} else {
						System.out.println("Veuillez renseigner un entier positif");
						scan = new Scanner(System.in);
					}
				} 
				break;
			case 2:
				System.out.println("Renseignez le type d'oracle voulu :");
				System.out.println("0: Zero");
				System.out.println("1: One");
				System.out.println("2: Random");
				System.out.println("3: Balanced");
				System.out.println("4: Constant");
				scan = new Scanner(System.in);
				state = 3;
				break;
			case 3:
				if (scan.hasNext()) {
					if (scan.hasNextInt()) {
						int option = scan.nextInt();
						switch(option) {
							case 0:
								this.settings.setType(OracleType.ZERO);
								state = 4;
								break;
							case 1:
								this.settings.setType(OracleType.ONE);
								state = 4;
								break;
							case 2:
								this.settings.setType(OracleType.RANDOM);
								state = 4;
								break;
							case 3:
								this.settings.setType(OracleType.BALANCED);
								state = 4;
								break;
							case 4:
								this.settings.setType(OracleType.CONSTANT);
								state = 4;
								break;
							default:
								System.out.println("Veuillez renseigner un entier de 0 a 4 inclus");
								scan = new Scanner(System.in);
								break;
						}
					} else {
						System.out.println("Veuillez renseigner un entier de 0 a 4 inclus");
						scan = new Scanner(System.in);
					}
				}
				break;
			case 4:
				System.out.println("Veuillez maintenant choisir le nombre d'agents d'analyse que vous desirez avoir : ");
				scan = new Scanner(System.in);
				state = 5;
				break;
			case 5:
				if (scan.hasNext()) {
					if (scan.hasNextInt()) {
						int b = scan.nextInt();
						if (b > 0) {
							this.settings.setNbAnalysis(b);
							state = 6;
						} else {
							System.out.println("Veuillez renseigner un entier positif");
							scan = new Scanner(System.in);
						}	
					} else {
						System.out.println("Veuillez renseigner un entier positif");
						scan = new Scanner(System.in);
					}
				}
				break;
			case 6:
				demarrage();
				state = 7;
				break;
		}
	}
	
	
	private void demarrage() {
		System.out.println("Felicitations ! Alice a ete initialisee !!");
		((AliceAgent) super.myAgent).creationCircuit(this.settings);
	}

	@Override
	public boolean done() {
		return (state == 7);
	}

}
