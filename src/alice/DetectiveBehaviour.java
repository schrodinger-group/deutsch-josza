package alice;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;

public class DetectiveBehaviour extends Behaviour{
	Boolean ok = false;
	
	public DetectiveBehaviour(Agent agent) {
		super(agent);
	}
	
	// Lance le traitement une fois le message re�u
	@Override
	public void action() {
		ACLMessage message = this.myAgent.receive();
		
		if (message != null) {
			traitement(message);
		} else {
			block();
		}
		
	}
	
	// Lis le message re�u et affiche le r�sultat en fonction
	private void traitement(ACLMessage message) {
		String resultante = Integer.toBinaryString(Integer.parseInt(message.getContent()));
		
		System.out.println("Mesure : " + resultante);
		
		if ((resultante.length() <= 1) || (resultante.substring(0, resultante.length()-1).equals("0"))) {
			System.out.println("La fonction est constante");
		} else {
			System.out.println("La fonction est Equilibree");
		}		
		
		this.ok = true;

	}

	@Override
	public boolean done() {
		return ok;
	}

}
