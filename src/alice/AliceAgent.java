package alice;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SequentialBehaviour;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;
import utils.AlgoSettings;

public class AliceAgent extends Agent{
	
	int n;
	
	public AliceAgent() {
		super();
		this.n = 0;
	}
	
	protected void setup(){
		SequentialBehaviour seq = new SequentialBehaviour();
		seq.addSubBehaviour(new InitBehaviour(this));
		seq.addSubBehaviour(new StartBehaviour(this));
		seq.addSubBehaviour(new DetectiveBehaviour(this));
		
		addBehaviour(seq);
	}
	
	public void creationCircuit(AlgoSettings setting) {
		Object options[] = {setting, this.getAID()}; 
		this.setN(setting.getSize());
		try {
			AgentController circuit = this.getContainerController().createNewAgent("CircuitAgent", "circuit.CircuitAgent", options);
			circuit.start();
		} catch (StaleProxyException e) {
			e.printStackTrace();
		}
	}
	
	public int getN() {
		return this.n;
	}
	
	public void setN(int n) {
		this.n = n;
	}
	
	public void restart() {
		SequentialBehaviour seq = new SequentialBehaviour();
		seq.addSubBehaviour(new InitBehaviour(this));
		seq.addSubBehaviour(new StartBehaviour(this));
		seq.addSubBehaviour(new DetectiveBehaviour(this));
		
		addBehaviour(seq);
	}
}
