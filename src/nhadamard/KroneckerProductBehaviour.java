package nhadamard;

import java.util.ArrayList;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;

public class KroneckerProductBehaviour extends OneShotBehaviour {

	ArrayList<ArrayList<ArrayList<Double>>> matList; 
	
	public KroneckerProductBehaviour(Agent a, ArrayList<ArrayList<ArrayList<Double>>> matList) {
		super.myAgent = a;
		this.matList=matList;
	}
	
	@Override
	public void action() {
		
		ArrayList<ArrayList<Double>> result = kroneckerProduct(this.matList);
		((NHadamardAgent) super.myAgent).gate=result;
		((NHadamardAgent) super.myAgent).addBehaviour(new passingThroughHadamardBehaviour(super.myAgent));
		
	}
	
	public ArrayList<ArrayList<Double>> kroneckerProduct(ArrayList<ArrayList<ArrayList<Double>>> matList){
			
			if(matList.size()<2) return null;
			else { 
				
				if (matList.size()>2) {
					
						ArrayList<ArrayList<ArrayList<Double>>> temp= new ArrayList<>();	
						temp.add(matList.remove(0));
						temp.add(kroneckerProduct(matList));
						return kroneckerProduct(temp);
					}
					
			else {
			
				ArrayList<ArrayList<Double>> mat1 = matList.get(0);
				ArrayList<ArrayList<Double>> mat2 = matList.get(1);
				ArrayList<ArrayList<Double>> mat3 = new ArrayList<>();
				
				
				for(int i=0; i<mat1.size()*mat2.size(); i++) {
					
					mat3.add(new ArrayList<Double>());
					
					for(int j=0; j<mat1.get(0).size()*mat2.get(0).size(); j++) {
			
						mat3.get(i).add(j, mat1.get(i/mat2.size()).get(j/mat2.get(0).size())*mat2.get(i%mat2.size()).get(j%mat2.get(0).size()));
					
					}
				}
				
				
				return mat3;
				
				}
			}
				
		}

	
}
