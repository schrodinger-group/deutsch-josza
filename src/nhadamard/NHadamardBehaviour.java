package nhadamard;

import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.ObjectMapper;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.lang.acl.ACLMessage;
import jade.proto.AchieveREInitiator;

public class NHadamardBehaviour extends AchieveREInitiator {

	public NHadamardBehaviour(Agent a, ACLMessage msg) {
		super(a, msg);
	}

	public void handleInform(ACLMessage response){
		
		if (response != null) {
			
			ObjectMapper mapper = new ObjectMapper();
			try {
				
				ArrayList<ArrayList<ArrayList<Double>>> list = mapper.readValue(response.getContent(), mapper.getTypeFactory().constructCollectionType(ArrayList.class, mapper.getTypeFactory().constructCollectionType(ArrayList.class, mapper.getTypeFactory().constructCollectionType(ArrayList.class, Double.class))));
				ArrayList<ArrayList<Double>> identity = list.get(0);
				ArrayList<ArrayList<Double>> hadamard = list.get(1);
				
				ArrayList<ArrayList<ArrayList<Double>>> matList = new ArrayList<>();

				for(int i=0; i< ((NHadamardAgent) super.myAgent).dimension ; i++) {
					matList.add(hadamard);
				}
				
				matList.add(identity);
				
				((NHadamardAgent) super.myAgent).addBehaviour(new KroneckerProductBehaviour(super.myAgent, matList));
				
			} catch (IOException e) {
				e.printStackTrace();
			} 
		}
		
		
	}

}
