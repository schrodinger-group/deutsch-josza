package nhadamard;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;



	//Initialiser matrice NHadamard
	//Multiplier états intriqués qubits 
	// et pour chaque analyse REAchieveInitiator
	//Appeler Mesure avec 2^n 
	
	// Dans le constructeur récupérer arguments n et nickname de kronecker 


public class NHadamardAgent extends Agent {

	int dimension;
	ArrayList<ArrayList<Double>> gate;
	String agentMesure;
	
	public void setup() {
		
		Object args[] = getArguments();
		this.agentMesure = ((String) args[0]);
		this.dimension = ((Integer) args[1]); 
		

		ACLMessage message = new ACLMessage (ACLMessage.REQUEST);
		ObjectMapper data = new ObjectMapper();
		
		ArrayList<String> content = new ArrayList();
		content.add("identity");
		content.add("hadamard");
		
		DFAgentDescription dfd = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		sd.setType("MatrixMap");
		dfd.addServices(sd);
		DFAgentDescription[] result;
		try {
			ObjectMapper mapper = new ObjectMapper();
			message.setContent(mapper.writeValueAsString(content));
			result = DFService.search(this, dfd);
			message.addReceiver(result[0].getName());
			addBehaviour(new NHadamardBehaviour(this, message));
			
		} catch (FIPAException | JsonProcessingException e) {
			e.printStackTrace();
		}
	}
	
}	
