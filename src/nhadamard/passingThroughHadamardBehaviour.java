package nhadamard;

import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.ObjectMapper;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import utils.IntricatedQubits;

// attends messages qubits, ajoute la colonne à la matrice Hn 
// et envoie à l'agent multiplication 

public class passingThroughHadamardBehaviour extends CyclicBehaviour {

	public passingThroughHadamardBehaviour(Agent a) {
		super.myAgent = a;
	}
	
	@Override
	public void action() {
		
		ACLMessage msg = super.myAgent.receive(MessageTemplate.MatchPerformative(ACLMessage.REQUEST));
		if (msg != null) {
			
			ObjectMapper mapper = new ObjectMapper();
			try {
			
				IntricatedQubits qubits = mapper.readValue(msg.getContent(), IntricatedQubits.class);
				ArrayList<ArrayList<Double>> mult = ((ArrayList<ArrayList<Double>>) ((NHadamardAgent) super.myAgent).gate.clone());
				
				mult.add(qubits.coeffs);
				
				ACLMessage message = new ACLMessage(ACLMessage.REQUEST);
				
				message.setContent(mapper.writeValueAsString(mult));
				
				DFAgentDescription dfd = new DFAgentDescription();
				ServiceDescription sd = new ServiceDescription();
				sd.setType("Multiplication");
				dfd.addServices(sd);
				DFAgentDescription[] result = DFService.search(super.myAgent, dfd);
				message.addReceiver(result[0].getName());
				
				
				// créer sous behaviour achieverRE qui envoie resultat mult à Mesure
				super.myAgent.addBehaviour(new WaitForMultiplicationBehaviour(super.myAgent, message));
				
			} catch (IOException e) {
				e.printStackTrace();
			} catch (FIPAException e) {
				e.printStackTrace();
			}
		
		}

	}

}
