package nhadamard;

import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.ObjectMapper;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.proto.AchieveREInitiator;
import oracle.OracleAgent;
import utils.IntricatedQubits;

public class WaitForMultiplicationBehaviour extends AchieveREInitiator {

	public WaitForMultiplicationBehaviour(Agent a, ACLMessage msg) {
		super(a, msg);
	}
	
	
	public void handleInform(ACLMessage msg) {
		if(msg != null) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				ArrayList<Double> coeffs = mapper.readValue(msg.getContent(), mapper.getTypeFactory().constructCollectionType(ArrayList.class, Double.class));
				IntricatedQubits qubits = new IntricatedQubits(coeffs);
				System.out.println("Phi3 = " + qubits.qubitsToString());
				ACLMessage message = new ACLMessage(ACLMessage.REQUEST);
				message.addReceiver(new AID(((NHadamardAgent) super.myAgent).agentMesure, AID.ISLOCALNAME));
				message.setContent(mapper.writeValueAsString(qubits));
				super.myAgent.send(message);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
}
