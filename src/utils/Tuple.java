package utils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Tuple<S> implements Serializable {
	private List<S> list1;
	private List<S> list2;
	
	public Tuple(){
		list1 = new ArrayList<>();
		list2 = new ArrayList<>();
	}
	
	public List<S> getList1(){
		return list1;
	}
	public void setList1(List<S> list1){
		this.list1 = list1;
	}
	
	public List<S> getList2(){
		return list2;
	}
	public void setList2(List<S> list2){
		this.list2 = list2;
	}
	
	public S getElementList1(int index) {
		return list1.get(index);
	}
	public S getElementList2(int index) {
		return list2.get(index);
	}
	
	public Duo<S> getElement(int index){
		return new Duo<S>(getElementList1(index), getElementList2(index));
	}
	
}
