package utils;

import java.util.ArrayList;

public class IntricatedQubits {
	public ArrayList<Double> coeffs;
	
	public IntricatedQubits() {
		this.coeffs = new ArrayList<>();
	}
	
	public IntricatedQubits(ArrayList<Double> coeff) {
		this.coeffs = coeff;
	}
	
	public boolean verifierCoeff() {
		double sum = 0;
		for (double i : coeffs) {
			sum += i;
		}
		int length = this.coeffs.size();
		return (sum == 1.0 && (length & (length - 1)) == 0);
	}
	
	public String qubitsToString() {
		int length = this.coeffs.size();
		int k = log2(length);
		String str = new String(), state = new String();
		for (int i=0;i<length;i++) {
			state = Integer.toBinaryString(i);
			while (state.length() < k)
				state = "0" + state;
			str += Double.toString(coeffs.get(i)) + " |" + state + ">";
			if (i < length-1) 
				str += " + ";
		}
		return str;
	}
	
	private int log2(int n){
	    if(n <= 0) throw new IllegalArgumentException();
	    return 31 - Integer.numberOfLeadingZeros(n);
	}
}