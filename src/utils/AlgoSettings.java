package utils;

// classe renseignant les parametres pour l'algorithme (type de fonction pour l'oracle, nombre de qubits, nombre d'agents analyse, etc
public class AlgoSettings {
	public enum OracleType {
		RANDOM,
		CONSTANT,
		BALANCED,
		ZERO,
		ONE
	}
	
	private int size;
	private OracleType type;
	private int nbAnalysis;
	
	public AlgoSettings() {
		this.size = 2;
		this.type = OracleType.RANDOM;
		this.nbAnalysis = 2;
	}
	
	public AlgoSettings(int n, OracleType t, int nb) {
		this.size = n;
		this.type = t;
		this.nbAnalysis = nb;
	}
	
	public int getSize() {
		return size;
	}
	
	public OracleType getType() {
		return type;
	}
	
	public int getNbAnalysis() {
		return nbAnalysis;
	}
	
	public void setSize(int n) {
		this.size = n;
	}
	
	public void setType(OracleType t) {
		this.type = t;
	}
	
	public void setNbAnalysis(int nb) {
		this.nbAnalysis = nb;
	}
	
	public String toString() {
		String result = "Type d'oracle : " + type.toString() + "\n";
		result = result + "N choisi : " + size + "\n";
		result = result + "Nombre d'agents analyse : " + nbAnalysis;
		return result;
		
	}
 }
