package utils;
import java.io.Serializable;

public class Duo<S> implements Serializable{
	private S first;
	private S second;
	
	public Duo(S first, S second) {
		this.first = first;
		this.second = second;
	}
	
	public S getFirst() {
		return this.first;
	}
	public void setFirst(S first) {
		this.first = first;
	}
	
	public S getSecond() {
		return this.second;
	}
	public void setSecond(S second) {
		this.second = second;
	}
		
}
