package matrixmap; 

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

public class MatrixMapAgent extends Agent {
	
	Map<String, ArrayList<ArrayList<Double>>> library;
	
	
	public MatrixMapAgent() {
		
		ArrayList<ArrayList<Double>> hadamard = new ArrayList<>();
		hadamard.add(new ArrayList<Double>(Arrays.asList(new Double[] { 1.0/Math.sqrt(2), 1.0/Math.sqrt(2) } )));
		hadamard.add(new ArrayList<Double>(Arrays.asList(new Double[] { 1.0/Math.sqrt(2), -1.0/Math.sqrt(2) } )));
		
		ArrayList<ArrayList<Double>> identity = new ArrayList<>();
		identity.add(new ArrayList<Double>(Arrays.asList(new Double[] { 1.0, 0.0 } )));
		identity.add(new ArrayList<Double>(Arrays.asList(new Double[] { 0.0, 1.0 } )));
	
		ArrayList<ArrayList<Double>> paulix = new ArrayList<>();
		paulix.add(new ArrayList<Double>(Arrays.asList(new Double[] { 0.0, 1.0 } )));
		paulix.add(new ArrayList<Double>(Arrays.asList(new Double[] { 1.0, 0.0 } )));
	
		
		library=new HashMap<String, ArrayList<ArrayList<Double>>>();
		library.put("hadamard", hadamard);
		library.put("identity", identity);
		library.put("paulix", paulix);
	
	}
	
	public void setup() {
		
		this.inscription();
		addBehaviour(new LibraryBehaviour(this));
		
	}
	
	private void inscription() {
		DFAgentDescription dfad = new DFAgentDescription();
		dfad.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("MatrixMap");
		sd.setName(getLocalName());
		dfad.addServices(sd);
		try { 
			DFService.register(this, dfad);
		} catch (FIPAException fe) {
			fe.printStackTrace(); 
		}
	}
	
	protected void takeDown() {
       try { DFService.deregister(this); }
       catch (Exception e) {}
    }



}
