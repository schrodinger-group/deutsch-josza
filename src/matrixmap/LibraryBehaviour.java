package matrixmap;

import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class LibraryBehaviour extends CyclicBehaviour {

	public LibraryBehaviour(Agent a) {
		super.myAgent=a;
	} 
	
	@Override
	public void action() {
		
		ACLMessage msg = super.myAgent.receive(MessageTemplate.MatchPerformative(ACLMessage.REQUEST));
		if (msg != null) {
			
			ObjectMapper mapper = new ObjectMapper();
			try {
				
				ArrayList<String> query = mapper.readValue(msg.getContent(),  mapper.getTypeFactory().constructCollectionType(ArrayList.class, String.class));
				
				ACLMessage message = msg.createReply();
				message.setPerformative(ACLMessage.INFORM);
				
				try {
					
					ArrayList<ArrayList<ArrayList<Double>>> answer = new ArrayList<>();
					
					for(String q : query)
						answer.add(((MatrixMapAgent)super.myAgent).library.get(q));
					 
					
					message.setContent(mapper.writeValueAsString(answer));
					super.myAgent.send(message);
					
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				} 
				
			} catch (IOException e) {
				e.printStackTrace();
			} 
					
		} else block(); 
		
	}

}
