package circuit;

import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.wrapper.StaleProxyException;

public class CreateMatrixMultiplicationAgent extends OneShotBehaviour {

	public CreateMatrixMultiplicationAgent(Agent agent) {
		super.myAgent = agent;
	}
	
	@Override
	public void action() {
		try {
			super.myAgent.getContainerController().createNewAgent("multiplication", "multiplication.MultiplicationAgent", null).start();
		} catch (StaleProxyException e) {
			e.printStackTrace();
		}
	}

}
