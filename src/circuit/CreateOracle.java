package circuit;

import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.wrapper.StaleProxyException;
import utils.AlgoSettings;

public class CreateOracle extends OneShotBehaviour {

	public CreateOracle(Agent agent) {
		super.myAgent = agent;
	}
	
	@Override
	public void action() {
		AlgoSettings.OracleType type = ((CircuitAgent)super.myAgent).getSettings().getType();
		int n = ((CircuitAgent)super.myAgent).getSettings().getSize();
		Object[] arg = {"hadamard", n, ((CircuitAgent)super.myAgent).getConsoleAgent(), "nhadamard", type};
		try {
			super.myAgent.getContainerController().createNewAgent("oracle", "oracle.OracleAgent", arg).start();
		} catch (StaleProxyException e) {
			e.printStackTrace();
		}
	}

}
