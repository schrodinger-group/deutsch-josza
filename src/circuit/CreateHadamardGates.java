package circuit;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.wrapper.StaleProxyException;

public class CreateHadamardGates extends OneShotBehaviour {

	public CreateHadamardGates(Agent agent) {
		super.myAgent = agent;
	}
	
	@Override
	public void action() {
		int n = ((CircuitAgent)super.myAgent).getSettings().getSize();
		Object[] arg = {"oracle", "matrixMap"};
		try {
			for (int i=0; i<=n; i++) {
				super.myAgent.getContainerController().createNewAgent("hadamard"+Integer.toString(i), "hadamard.HadamardAgent", arg).start();
			}
		} catch (StaleProxyException e) {
			e.printStackTrace();
		}
	}
}
