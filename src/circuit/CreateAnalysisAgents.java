package circuit;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.wrapper.StaleProxyException;

public class CreateAnalysisAgents extends OneShotBehaviour {

	public CreateAnalysisAgents(Agent agent) {
		super.myAgent = agent;
	}
	
	@Override
	public void action() {
		int nb = ((CircuitAgent)super.myAgent).getSettings().getNbAnalysis();
		try {
			for (int i=0; i<nb; i++) {
				super.myAgent.getContainerController().createNewAgent("analysis"+Integer.toString(i), "analyse.AnalysisAgent", null).start();
			}
		} catch (StaleProxyException e) {
			e.printStackTrace();
		}
	}
}
