package circuit;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.wrapper.StaleProxyException;
import utils.AlgoSettings;

public class WaitForSettings extends Behaviour {

	private boolean msgReceived = false;
	
	public WaitForSettings(Agent a) {
		super.myAgent=a;
	}
	
	@Override
	public void action() {		
		ACLMessage msg = super.myAgent.receive(MessageTemplate.MatchPerformative(ACLMessage.REQUEST));
		if (msg != null) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				((CircuitAgent)super.myAgent).setSettings(mapper.readValue(msg.getContent(), AlgoSettings.class));
				((CircuitAgent)super.myAgent).setConsoleAgent(msg.getSender());
				msgReceived = true;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean done() {
		return msgReceived;
	}
		
}
