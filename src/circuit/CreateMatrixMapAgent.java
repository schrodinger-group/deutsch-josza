package circuit;

import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.wrapper.StaleProxyException;

public class CreateMatrixMapAgent extends OneShotBehaviour {

	public CreateMatrixMapAgent(Agent agent) {
		super.myAgent = agent;
	}
	@Override
	public void action() {
		try {
			super.myAgent.getContainerController().createNewAgent("matrixMap", "matrixmap.MatrixMapAgent", null).start();
		} catch (StaleProxyException e) {
			e.printStackTrace();
		}
	}

}
