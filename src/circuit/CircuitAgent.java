package circuit;
import java.util.ArrayList;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SequentialBehaviour;
import jade.wrapper.StaleProxyException;
import utils.AlgoSettings;


// http://dictionnaire.sensagent.leparisien.fr/Produit%20de%20Kronecker/fr-fr/ 

public class CircuitAgent extends Agent {
	
	private AlgoSettings settings;
	private AID consoleAgent;
	
	public void setup(){
		
		Object args[] = getArguments();
		
		this.settings = (AlgoSettings) args[0];
		
		this.consoleAgent = (AID) args[1];
		
		
		SequentialBehaviour seq = new SequentialBehaviour();
		seq.addSubBehaviour(new CreateMatrixMapAgent(this));
		seq.addSubBehaviour(new CreateAnalysisAgents(this));
		seq.addSubBehaviour(new CreateMatrixMultiplicationAgent(this));
		seq.addSubBehaviour(new CreateNMesureGate(this));
		seq.addSubBehaviour(new CreateNHadamardGate(this));
		seq.addSubBehaviour(new CreateOracle(this));
		seq.addSubBehaviour(new CreateHadamardGates(this));
		seq.addSubBehaviour(new SendOkToConsole(this));
		
		addBehaviour(seq);
	}

	public AlgoSettings getSettings() {
		return settings;
	}

	public void setSettings(AlgoSettings settings) {
		this.settings = settings;
	}

	public AID getConsoleAgent() {
		return consoleAgent;
	}

	public void setConsoleAgent(AID consoleAgent) {
		this.consoleAgent = consoleAgent;
	}
		
}
