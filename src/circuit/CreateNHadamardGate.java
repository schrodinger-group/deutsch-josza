package circuit;

import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.wrapper.StaleProxyException;

public class CreateNHadamardGate extends OneShotBehaviour {

	public CreateNHadamardGate(Agent agent) {
		super.myAgent = agent;
	}
	@Override
	public void action() {
		int n = ((CircuitAgent)super.myAgent).getSettings().getSize();
		Object[] arg = {"mesure", n, "multiplication", "matrixMap"};
		try {
			super.myAgent.getContainerController().createNewAgent("nhadamard", "nhadamard.NHadamardAgent", arg).start();
		} catch (StaleProxyException e) {
			e.printStackTrace();
		}
	}

}
