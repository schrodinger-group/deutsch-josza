package circuit;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.wrapper.StaleProxyException;

public class CreateNMesureGate extends OneShotBehaviour {

	public CreateNMesureGate(Agent agent) {
		super.myAgent = agent;
	}
	
	@Override
	public void action() {
		AID console = ((CircuitAgent)super.myAgent).getConsoleAgent();
		Object[] arg = {console};
		try {
			super.myAgent.getContainerController().createNewAgent("mesure", "mesure.MesureAgent", arg).start();
		} catch (StaleProxyException e) {
			e.printStackTrace();
		}
	}
}
