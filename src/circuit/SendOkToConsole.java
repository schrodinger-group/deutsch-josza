package circuit;

import java.util.ArrayList;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import utils.AlgoSettings;

public class SendOkToConsole extends OneShotBehaviour {
	
	public SendOkToConsole(Agent agent) {
		super.myAgent = agent;
	}
	
	@Override
	public void action() {
		ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
		msg.addReceiver(((CircuitAgent)super.myAgent).getConsoleAgent());
		ArrayList<String> circuitInit = new ArrayList<String>();
		AlgoSettings settings = ((CircuitAgent)super.myAgent).getSettings();
		int n = settings.getSize();
		ArrayList<String> registers = new ArrayList<>();
		for (int i=0; i<=n; i++)
			registers.add("hadamard"+Integer.toString(i));
		ObjectMapper mapper = new ObjectMapper();
		try {
			msg.setContent(mapper.writeValueAsString(registers));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		super.myAgent.send(msg);
	}

}
